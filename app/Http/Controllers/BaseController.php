<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Response;

trait BaseController
{
    function success(string $message = "", mixed $data = null, int $code = 200, ...$meta)
    {
        $response = [
            "success" => true,
            "message" => $message,
            "data" => $data,
            "code" => $code,
            ...$meta
        ];

        return Response::json($response, $code);
    }

    protected function error(string $message = '', array|object $data = [], int $code = 403)
    {
        return Response::json([
            'success' => false,
            'message' => $message,
            "data" => $data,
            "code" => $code
        ], $code);
    }

}
